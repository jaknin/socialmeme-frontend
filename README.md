# SocialMeme-Frontend

Frontend of SocialMeme®

To be able to run this project, you have to install [node]("https://nodejs.org/en/download/") first.

After that, you have to install the angular tools globally on your computer, to archive this, you can use following command:

```batch
npm install -g @angular/cli
```
After that, you have to install all packages, you can archive this with following command:
```batch
npm install
```


After that you're set to go, you can simply run the server with following command:
```batch
ng serve
```